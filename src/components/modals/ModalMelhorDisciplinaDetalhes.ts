import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular'

@Component({
  templateUrl: './ModalMelhorDisciplinaDetalhes.html'
})

export class ModalMelhorDisciplinaDetalhes{
  public equipe;
  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController){
    this.equipe = params.get('equipe');
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }


}
