import { Component, ViewChild } from '@angular/core';
import { Platform, NavParams, NavController, ViewController, ModalController } from 'ionic-angular';
import { CompeticaoService } from '../../../providers/competicao-service';
@Component({
  templateUrl: './ModalCarteira.html',
  selector: 'modal-carteira'
})

export class ModalCarteira{
  goleiro: any;
  validade: any;
  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController, public competicaoService: CompeticaoService){

  }

  ionViewDidLoad(){
    let id = this.params.get('jogador');
    this.competicaoService.findGoleiro(id)
    .subscribe((goleiro)=>{
      this.goleiro = goleiro[0];
      console.table({"goleiro": this.goleiro});
      console.log(this.goleiro.img_perfil);
      let date = new Date();
      this.validade = '31/12/'+date.getFullYear();
    },(error)=>console.log(error));
  }

  dismiss(){
    this.goleiro = null;
    this.validade = null;
    this.viewCtrl.dismiss();
  }
}
