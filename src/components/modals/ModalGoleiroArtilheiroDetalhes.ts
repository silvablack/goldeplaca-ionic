import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular'

@Component({
  templateUrl: './ModalGoleiroArtilheiroDetalhes.html'
})

export class ModalGoleiroArtilheiroDetalhes{
  public jogador;
  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController){
    this.jogador = params.get('jogador');
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }


}
