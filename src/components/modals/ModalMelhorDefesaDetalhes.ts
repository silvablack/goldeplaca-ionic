import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular'

@Component({
  templateUrl: './ModalMelhorDefesaDetalhes.html'
})

export class ModalMelhorDefesaDetalhes{
  public equipe;
  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController){
    this.equipe = params.get('equipe');
    console.log(this.equipe);
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }


}
