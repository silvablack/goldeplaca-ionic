import { Component, ViewChild } from '@angular/core';
import { Platform, NavParams, NavController, ViewController } from 'ionic-angular';
import { CompeticaoService } from '../../../providers/competicao-service';

@Component({
  templateUrl: './ModalDetalhesJogo.html',
  selector: 'modal-detalhes-jogo'
})

export class ModalDetalhesJogo{
  public jogo: any;
  public sumula: any;
  public arbitro: any;
  public equipe1: any;
  public equipe2: any;
  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController,public competicaoService: CompeticaoService,){
    let jogo = params.get('jogo')[0]
    console.table({'ModaldetalheJogo':jogo});
    this.arbitro = jogo.arbitro;
    this.sumula = jogo.sumula;
    this.jogo = jogo.jogo;
    this.equipe1 = jogo.detalhes_equipe1;
    this.equipe2 = jogo.detalhes_equipe2;

  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

}
