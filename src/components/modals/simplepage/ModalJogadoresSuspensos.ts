import { Component } from '@angular/core';
import { Platform, NavParams, ViewController, Events } from 'ionic-angular'

@Component({
  templateUrl: './ModalJogadoresSuspensos.html'
})

export class ModalJogadoresSuspensos{

  public jogadores: any;

  constructor(public events: Events, public platform: Platform,public params: NavParams,public viewCtrl: ViewController){
    this.jogadores = params.get('jogadores');
    console.log(this.jogadores);
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

  openJogadorDetalhes(n,c,i){
    this.events.publish('jogadordetalhes:selected',n,c,i);
  }


}
