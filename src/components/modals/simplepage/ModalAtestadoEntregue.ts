import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular'

@Component({
  templateUrl: './ModalAtestadoEntregue.html'
})

export class ModalAtestadoEntregue{
  public jogadores;
  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController){
    this.jogadores = params.get('jogadores');
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }


}
