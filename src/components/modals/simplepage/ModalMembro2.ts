import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular'

@Component({
  templateUrl: './ModalMembro2.html'
})

export class ModalMembro2{
  public jogadores;
  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController){
    this.jogadores = params.get('jogadores');
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }


}
