import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';

@Component({
  selector: 'example-app',
  template: `
  <div>
      <label>Arquivo em PDF</label>
      <input type="text" placeholder="Arquivo em PDF" [(ngModel)]="baseSrc">
  </div>
  <div>
      <label>Página:</label>
      <input type="number" placeholder="Page" [(ngModel)]="page">
  </div>
  <pdf-viewer [src]="baseSrc"
              [page]="page"
              [original-size]="true"
              style="display: block;"
  ></pdf-viewer>
  `
})
export class ModalViewPdf {
  baseSrc: string;
  page: number = 1;
  constructor(public params: NavParams,private transfer: FileTransfer){
    const fileTransfer: FileTransferObject = transfer.create();
    let name = params.get('name');
    fileTransfer.download(params.get('path'), 'assets/' + name).then((entry) => {
      console.log('download complete: ' + entry.toURL());
      this.baseSrc='assets/' + name;
    }, (error) => {
      // handle error
      console.table({error: error,name: name, path: params.get('path')});
    });
  }
}
