import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular'

@Component({
  templateUrl: './ModalAniversariantes.html'
})

export class ModalAniversariantes{
  public jogadores;
  public data;
  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController){
    this.jogadores = params.get('jogadores');
    let date = new Date();
    this.data = date.getDate()+'/'+date.getMonth();
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }


}
