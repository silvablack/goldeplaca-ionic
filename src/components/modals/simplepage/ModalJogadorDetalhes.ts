import { Component } from '@angular/core';
import { Platform, NavParams, ViewController, LoadingController } from 'ionic-angular'
import { CompeticaoService } from '../../../providers/competicao-service';
import 'rxjs/add/operator/finally';

@Component({
  templateUrl: './ModalJogadorDetalhes.html'
})

export class ModalJogadorDetalhes{
  public data: Array<any>;
  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController, public loading: LoadingController, public competicaoService: CompeticaoService){
    let showloading = loading.create({content:'Carregando...'});
    showloading.present();
    competicaoService.findJogadorDetalhes(params.get('nome'),params.get('categoria'),params.get('id'))
    .finally(()=>{
      showloading.dismiss();
    })
    .subscribe((response)=>{
      this.data = response[0];
      console.table({responseJogadorDetalhes: response[0]});
      console.log(this.data);
    },(error)=>{
      console.table({errorJogadorDetalhes: error});
    })

  }
  adversario(jogo): any{
    for(let x of this.data['jogo']){
      if(x.id_jogo === jogo)
        return x.adversario;
    }
    return false;
  }

  datajogo(jogo): any{
    for(let x of this.data['jogo']){
      if(x.id_jogo === jogo)
        return x.data_jogo;
    }
    return false;
  }

  numerojogo(jogo): any{
    for(let x of this.data['jogo']){
      if(x.id_jogo === jogo)
        return x.njg;
    }
    return false;
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }
}
