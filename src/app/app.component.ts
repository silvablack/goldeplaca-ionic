import { Component, ViewChild } from '@angular/core';
import { Nav, Platform,LoadingController,ToastController, ModalController, AlertController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Home } from '../pages/home/home';
import { Competicao } from '../pages/competicao/competicao';
import { Noticia } from '../pages/noticia/noticia';
import { Resultado } from '../pages/resultado/resultado';
import { TabelaJogos } from '../pages/tabelajogos/tabelajogos';
import { ConfigPage } from '../pages/config/config';
import { CompeticaoService } from '../providers/competicao-service';
import { Network } from '@ionic-native/network';
import { ModalJogadoresSuspensos } from '../components/modals/simplepage/ModalJogadoresSuspensos';
import { ModalJogadoresPendurados } from '../components/modals/simplepage/ModalJogadoresPendurados';
import { ModalBalancouRede } from '../components/modals/simplepage/ModalBalancouRede';
import { ModalMembro1 } from '../components/modals/simplepage/ModalMembro1';
import { ModalMembro2 } from '../components/modals/simplepage/ModalMembro2';
import { ModalAtestadoEntregue } from '../components/modals/simplepage/ModalAtestadoEntregue';
import { ModalAtestadoPendente } from '../components/modals/simplepage/ModalAtestadoPendente';
import { ModalAniversariantes } from '../components/modals/simplepage/ModalAniversariantes';
import { ModalPautaJulgamento } from '../components/modals/simplepage/ModalPautaJulgamento';
import { ModalJogosAdiados } from '../components/modals/simplepage/ModalJogosAdiados';
import { ModalJogosWo } from '../components/modals/simplepage/ModalJogosWo';
import { ModalDetalhesJogo } from '../components/modals/jogos/ModalDetalhesJogo';
import { ImgcacheService } from '../global/services/cache-img/cache-img.service';

import { NativeStorage } from '@ionic-native/native-storage';

import { OneSignal } from '@ionic-native/onesignal';

import { ModalViewPdf } from '../components/modals/simplepage/ModalViewPdf';

import { ModalJogadorDetalhes } from '../components/modals/simplepage/ModalJogadorDetalhes';

import 'rxjs/add/operator/finally';
import moment from 'moment';
import 'moment-timezone';
moment.tz.setDefault("America/Sao_Paulo");

@Component({
  templateUrl: 'app.html',
  providers: [CompeticaoService]
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  _ids: any;
  rootPage: any = Competicao;
  currentCompetitionPage: any = Competicao;
  idCompeticao: any;
  categoriaSelectedClass: any;
  categoriaSelectedJg: any;
  categoriaList: Array<any>;
  pages: Array<{title: string, component: any}>;
  competicoes: Array<any>;
  competitionsSelected = [];
  public load;

  constructor(public modal: ModalController,public events: Events,public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public competicaoService: CompeticaoService,
    public toastCtrl: ToastController,
    private net: Network,
    private imgcacheService: ImgcacheService,
    private nativeStorage: NativeStorage,
    public _oneSignal: OneSignal,
    public loading: LoadingController,
    public alert: AlertController
    ){
    this.load = this.loading.create({content:'', spinner:'dots'});
    this.load.present();
    this.initializeApp();
    events.subscribe('competition:selected',(id, name)=>{
      this.loadCategoria(id);
      this.idCompeticao = id;
      this.toast(name);
    });
    events.subscribe('detalhesjogos:selected',(id)=>{
      this.detalhes(id)
    });
    events.subscribe('clear:selected',(id)=>{
      this.categoriaSelectedJg = null;
    });
    events.subscribe('jogadordetalhes:selected',(nome,categoria,id)=>{
      let showmodal = modal.create(ModalJogadorDetalhes,{
        nome: nome,
        categoria: categoria,
        id: id
      });
    showmodal.present();
    });
  }

  detalhes(id){
    let loader = this.loading.create({content:'', spinner:'dots'});

    loader.present();

    this.competicaoService.findDetalhesJogo(id).finally(()=>{
      loader.dismiss();
    }).subscribe((data)=>{
      console.table({'detalheJogo':data});
      this.modal.create(ModalDetalhesJogo,{jogo:data}).present();
    }, (err) =>{
      console.log(err);
    })
  }

  displayNetworkUpdate(connectionState: string){
    let networkType = this.net.type;
    this.toastCtrl.create({
      message: 'Você está '+connectionState+' ('+networkType+')',
      duration: 3000
    }).present();
  }

  toast(msg){
    let toast = this.toastCtrl.create({
      message: msg,
      position: 'bottom',
      duration: 4000,
      showCloseButton: true,
      closeButtonText: 'x'
    });
    toast.present();
  }

  loadCategoria(competicao){
    let l = this.loading.create({content:'', spinner:'dots'});
    l.present();
    this.competicaoService.findCategoria(competicao)
    .finally(()=>{
      l.dismiss();
    })
    .subscribe(data=>{
      this.categoriaList = data;
    })
  }

  onChange(competicao){
    let l = this.loading.create({content:'', spinner:'dots'});
    l.present();
    this.competicaoService.findCategoria(competicao)
    .finally(()=>{
      l.dismiss();
    }).subscribe(data=>{
      this.categoriaList = data;
    })
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.checkNetworkConnection();
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.imgcacheService.initImgCache().then(() => {
        this.nativeStorage.getItem('current_competitions')
       .then((data)=>{
         let current: Array<any> = [];
         current.push(data);
         //VERIFICAR SE ALGUMA COMPETICAO FOI EXCLUIDA DO SERVIDOR E ENTAO EXCLUI NO APP
         this.verificarCompeticoes(data);

         console.table({currentCompetition: data});
         this._ids = data;
         //VERIFICAR SE EXISTE NOVA COMPETICAO
         this.competicaoService.findNew().subscribe((then)=>{
          console.table({findNew:then});
          if(then.lenght>0){
            for(let x of then){
              console.table({debug1: x, debug2: this.competicaoExisteStorage(x.attr.id)});
              if(!this.competicaoExisteStorage(x.attr.id)){
                current.push({id: x.attr.id, name: x.attr.nome_competicao});
                console.table({debug3: current});
                this.nativeStorage.setItem('current_competitions',current).then(then=>console.table({nativeStoragenew: then}, error=>console.table(error)));
              }
            }
          }
         });
         this.initOneSignalNotification(data);
         //console.log(data);
         if(this.load){ this.load.dismiss(); this.load = null; }
         this.nav.setRoot(this.currentCompetitionPage, {
           data: data
         });
         console.log('ENTROU AQUI');
       },(error)=>{
         console.log('ENTROU NA EXCESSAO');
         console.log(error);

         this.competicaoService.findAll().finally(()=>{
           if(this.load){ this.load.dismiss(); this.load = null; }
         }).subscribe((competicoes)=>{
           this.competicoes = competicoes;
           for(let x of competicoes){
             this.pushCompeticao(x.attr.id,x.attr.nome_competicao,null);
           }
           this.storageCompetitions();
         },(error)=>{console.log(error)});

         let alert = this.alert.create({
           title: 'Atenção',
           message: 'Caso queira realizar qualquer alteração na visualização de competições, vá em Menu > Alterar Competições',
           buttons: ['Ok']
         });
         alert.present();
         if(this.load){ this.load.dismiss(); this.load = null; }
         //this.nav.setRoot(this.rootPage);
       });
      });
    });
  }

  competicaoExiste(id){
    this.competicaoService.findOne(id).subscribe((c)=>{
      console.log(c);
      return c.lenght ? true:false;
    },(e)=>console.log(e));
    return false;
  }

  competicaoExisteStorage(id){
    this.nativeStorage.getItem('current_competitions').then((then)=>{
      for(let x of then){
        if(x.id === id){
          return true;
        }
      }
    })
  }

  excluirCompeticao(id){
    this.nativeStorage.remove(id);
  }

  verificarCompeticoes(data){
    console.table({log:data});
    for(let x of data){
      if(!this.competicaoExiste(x.id)){
        this.excluirCompeticao(x.id);
      }
    }
  }

  pushCompeticao(id,name,$event){
    //console.table({'id':id, 'event':$event});
    this.competitionsSelected.push({id: id, name: name});
    //console.log(this.competitionsSelected);
  }

  storageCompetitions(){
    if(this.competitionsSelected){
      let _ids = this.competitionsSelected;
      this.nativeStorage.setItem('current_competitions',_ids);
      this.nav.setRoot(Competicao);
    }else{
        this.alert.create({
          title: 'Atenção',
          message: 'Por favor, escolha uma alternativa',
          buttons: ['Ok']
        }).present();
    }
  }

  goHome():void{
     this.nav.setRoot(this.currentCompetitionPage, {
       data: this._ids
     });
  }

  goNoticias():void{
     this.nav.push(Noticia, {
       data: this.idCompeticao
     });
  }

  initOneSignalNotification(_ids){
    console.table({"notificacacao-ativada":"NOTIFICACAO ATIVADA"});
    let funcaoRetorno = (data) => {
       this.goHome();
    };
    let oneSignal = this._oneSignal;
    oneSignal.startInit("0e25b3a6-58d6-4d9d-bffd-6e42aa8180b0",
        "462392605788").handleNotificationOpened(funcaoRetorno);
        for(let x of _ids){
          oneSignal.sendTag(String(x.id),String(x.id));
          console.table({"id":x.id});
        }
    oneSignal.endInit();
    console.log(oneSignal);
  }

  checkNetworkConnection(){
    console.table({'net':this.net});
    let l = this.loading.create({content: 'Você está desconectado, por favor. Conecte-se a internet para usar o APP.'});
    this.net.onConnect().subscribe((data)=>{
      console.log(data);
      this.displayNetworkUpdate(data.type);

    }, error => console.log(error));
    this.net.onDisconnect().subscribe((data)=>{
      console.log(data);
      this.displayNetworkUpdate(data.type);

      l.present();
    }, error => console.log(error));
  }

  resultado(){
    let categoria = this.categoriaSelectedClass;
    let competicao = this.idCompeticao;
    this.categoriaSelectedClass = null;
    let loading = this.loading.create({content:'', spinner:'dots'});
    loading.present();
    this.competicaoService.findClassificacao(competicao,categoria)
    .finally(()=>{
      loading.dismiss();
    })
    .subscribe((data)=>{
      this.nav.push(Resultado, {
        data: data,
        competicao: competicao
      });
    });
  }

  tblJogos(){
    let categoria = this.categoriaSelectedJg;
    let competicao = this.idCompeticao;
    this.nav.push(TabelaJogos, {
      competicao: competicao,
      categoria: categoria
    });
  }

  changeCompetitions(){
    this.nav.push(ConfigPage);
  }

  openSimplePage(page){
    switch(page){
      case 'jdgs':this.getJogadoresSuspensos();
      break;
      case 'jgdp':this.getJogadoresPendurados();
      break;
      case 'blnr':this.getBalancouRede();
      break;
      case 'mem1':this.getMembro1();
      break;
      case 'mem2':this.getMembro2();
      break;
      case 'ptaj':this.getPautaJulgamento();
      break;
      case 'atsp':this.getAtestadoPendente();
      break;
      case 'atse':this.getAtestadoEntregue();
      break;
      case 'bday':this.getAniversariantes();
      break;
    }
  }

  getJogadoresSuspensos(){
    let loading = this.loading.create({content:'', spinner:'dots'})
    loading.present();
    this.competicaoService.findJogadoresSuspensos(this.idCompeticao)
    .finally(()=>{
      loading.dismiss();
    })
    .subscribe((then)=>{
      this.modal.create(ModalJogadoresSuspensos,{
          'jogadores': then
      }).present();
    },(error)=>{
      console.log(error);
    });
  }
  getJogadoresPendurados(){
    let loading = this.loading.create({content:'', spinner:'dots'})
    loading.present();
    this.competicaoService.findJogadoresPendurados(this.idCompeticao)
    .finally(()=>{
      loading.dismiss();
    })
    .subscribe((then)=>{
      this.modal.create(ModalJogadoresPendurados,{
          'jogadores': then
      }).present();
    },(error)=>{
      console.log(error);
    });
  }
  getBalancouRede(){
    let loading = this.loading.create({content:'', spinner:'dots'})
    loading.present();
    this.competicaoService.findBalancouRede(this.idCompeticao)
    .finally(()=>{
      loading.dismiss();
    })
    .subscribe((then)=>{
      this.modal.create(ModalBalancouRede,{
          'jogadores': then
      }).present();
    },(error)=>{
      console.log(error);
    });
  }
  getAtestadoPendente(){
    let loading = this.loading.create({content:'', spinner:'dots'})
    loading.present();
    this.competicaoService.findAtestadoPendente(this.idCompeticao)
    .finally(()=>{
      loading.dismiss();
    })
    .subscribe((then)=>{
      this.modal.create(ModalAtestadoPendente,{
          'jogadores': then
      }).present();
    },(error)=>{
      console.log(error);
    });
  }
  getAtestadoEntregue(){
    let loading = this.loading.create({content:'', spinner:'dots'})
    loading.present();
    this.competicaoService.findAtestadoEntregue(this.idCompeticao)
    .finally(()=>{
      loading.dismiss();
    })
    .subscribe((then)=>{
      this.modal.create(ModalAtestadoEntregue,{
          'jogadores': then
      }).present();
    },(error)=>{
      console.log(error);
    });
  }
  getMembro1(){
    let loading = this.loading.create({content:'', spinner:'dots'})
    loading.present();
    this.competicaoService.findMembro1(this.idCompeticao)
    .finally(()=>{
      loading.dismiss();
    })
    .subscribe((then)=>{
      this.modal.create(ModalMembro1,{
          'jogadores': then[0]
      }).present();
    },(error)=>{
      console.log(error);
    });
  }
  getMembro2(){
    let loading = this.loading.create({content:'', spinner:'dots'})
    loading.present();
    this.competicaoService.findMembro2(this.idCompeticao)
    .finally(()=>{
      loading.dismiss();
    })
    .subscribe((then)=>{
      this.modal.create(ModalMembro2,{
          'jogadores': then[0]
      }).present();
    },(error)=>{
      console.log(error);
    });
  }
  getPautaJulgamento(){
    let loading = this.loading.create({content:'', spinner:'dots'})
    loading.present();
    this.competicaoService.findPautaJulgamento(this.idCompeticao)
    .finally(()=>{
      loading.dismiss();
    })
    .subscribe((then)=>{
      this.modal.create(ModalPautaJulgamento,{
          'jogadores': then[0]
      }).present();
    },(error)=>{
      console.log(error);
    });
  }
  getAniversariantes(){
    let loading = this.loading.create({content:'', spinner:'dots'})
    loading.present();
    this.competicaoService.findAniversariantes(this.idCompeticao)
    .finally(()=>{
      loading.dismiss();
    })
    .subscribe((then)=>{
      this.modal.create(ModalAniversariantes,{
          'jogadores': then
      }).present();
    },(error)=>{
      console.log(error);
    });
  }

  getJogosAdiados(){
    let loading = this.loading.create({content:'', spinner:'dots'})
    loading.present();
    this.competicaoService.findJogosAdiados(this.idCompeticao)
    .finally(()=>{
      loading.dismiss();
    })
    .subscribe((then)=>{
      this.modal.create(ModalJogosAdiados,{
          'jogos': then
      }).present();
    },(error)=>{
      console.log(error);
    });
  }

  getJogosWo(){
    let loading = this.loading.create({content:'', spinner:'dots'})
    loading.present();
    this.competicaoService.findJogosWo(this.idCompeticao)
    .finally(()=>{
      loading.dismiss();
    })
    .subscribe((then)=>{
      this.modal.create(ModalJogosWo,{
          'jogos': then
      }).present();
    },(error)=>{
      console.log(error);
    });
  }

  getPdf(path){
    this.nav.push(ModalViewPdf,{
      'path': 'http://www.goldeplacama.com.br/'+path+this.idCompeticao+'.pdf',
      'name': path+this.idCompeticao+'.pdf'
    })
  }

}
