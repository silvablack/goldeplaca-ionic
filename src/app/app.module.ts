import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { PreHome } from '../pages/pre-home/pre-home';
import { Home } from '../pages/home/home';
import { Competicao } from '../pages/competicao/competicao';
import { Noticia } from '../pages/noticia/noticia';
import { Resultado } from '../pages/resultado/resultado';
import { TabelaJogos } from '../pages/tabelajogos/tabelajogos';
import { ConfigPage } from '../pages/config/config';

import { CompeticaoService } from '../providers/competicao-service';
import { Connectivity } from '../providers/connectivity';
import { SingletonService } from '../providers/singleton';

import { NativeStorage } from '@ionic-native/native-storage';

/*IMPORT MODALS*/
import { ModalClassificacao } from '../components/modals/classificacao/ModalClassificacao';
import { ModalArtilheiroDetalhes } from '../components/modals/ModalArtilheiroDetalhes';
import { ModalMelhorDisciplinaDetalhes } from '../components/modals/ModalMelhorDisciplinaDetalhes';
import { ModalAtaquePositivoDetalhes } from '../components/modals/ModalAtaquePositivoDetalhes';
import { ModalMelhorDefesaDetalhes } from '../components/modals/ModalMelhorDefesaDetalhes';
import { ModalGolsContraDetalhes } from '../components/modals/ModalGolsContraDetalhes';
import { ModalGoleiroArtilheiroDetalhes } from '../components/modals/ModalGoleiroArtilheiroDetalhes';
import { ModalCarteira } from '../components/modals/goleiro/ModalCarteira';
import { ModalJogadoresSuspensos } from '../components/modals/simplepage/ModalJogadoresSuspensos';
import { ModalJogadoresPendurados } from '../components/modals/simplepage/ModalJogadoresPendurados';
import { ModalBalancouRede } from '../components/modals/simplepage/ModalBalancouRede';
import { ModalMembro1 } from '../components/modals/simplepage/ModalMembro1';
import { ModalMembro2 } from '../components/modals/simplepage/ModalMembro2';
import { ModalAtestadoEntregue } from '../components/modals/simplepage/ModalAtestadoEntregue';
import { ModalAtestadoPendente } from '../components/modals/simplepage/ModalAtestadoPendente';
import { ModalAniversariantes } from '../components/modals/simplepage/ModalAniversariantes';
import { ModalPautaJulgamento } from '../components/modals/simplepage/ModalPautaJulgamento';
import { ModalViewPdf } from '../components/modals/simplepage/ModalViewPdf';
import { ModalDetalhesJogo } from '../components/modals/jogos/ModalDetalhesJogo';
import { ModalJogadorDetalhes } from '../components/modals/simplepage/ModalJogadorDetalhes';
import { ModalNoticia } from '../components/modals/simplepage/ModalNoticia';

import { ModalJogosAdiados } from '../components/modals/simplepage/ModalJogosAdiados';
import { ModalJogosWo } from '../components/modals/simplepage/ModalJogosWo';

import { BrowserModule } from '@angular/platform-browser';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';

import { LazyImgComponent }   from '../global/components/lazy-img/lazy-img.component';

import { LazyLoadDirective }   from '../global/directives/lazy-load.directive';

import { ImgcacheService } from '../global/services/cache-img/cache-img.service';
import { OneSignal } from '@ionic-native/onesignal';

import { PdfViewerComponent } from 'ng2-pdf-viewer';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';

@NgModule({
  declarations: [
    MyApp,
    PreHome,
    Home,
    Competicao,
    Noticia,
    Resultado,
    TabelaJogos,
    ConfigPage,
    ModalClassificacao,
    ModalArtilheiroDetalhes,
    ModalMelhorDisciplinaDetalhes,
    ModalAtaquePositivoDetalhes,
    ModalMelhorDefesaDetalhes,
    ModalGolsContraDetalhes,
    ModalGoleiroArtilheiroDetalhes,
    ModalJogadoresSuspensos,
    ModalJogadoresPendurados,
    ModalBalancouRede,
    ModalMembro1,
    ModalMembro2,
    ModalAtestadoEntregue,
    ModalAtestadoPendente,
    ModalAniversariantes,
    ModalPautaJulgamento,
    LazyImgComponent,
    LazyLoadDirective,
    ModalDetalhesJogo,
    ModalCarteira,
    PdfViewerComponent,
    ModalViewPdf,
    ModalJogadorDetalhes,
    ModalJogosAdiados,
    ModalJogosWo,
    ModalNoticia
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Home,
    PreHome,
    Competicao,
    Noticia,
    Resultado,
    TabelaJogos,
    ConfigPage,
    ModalClassificacao,
    ModalArtilheiroDetalhes,
    ModalMelhorDisciplinaDetalhes,
    ModalAtaquePositivoDetalhes,
    ModalMelhorDefesaDetalhes,
    ModalGolsContraDetalhes,
    ModalGoleiroArtilheiroDetalhes,
    ModalDetalhesJogo,
    ModalCarteira,
    ModalJogadoresSuspensos,
    ModalJogadoresPendurados,
    ModalBalancouRede,
    ModalMembro1,
    ModalMembro2,
    ModalAtestadoEntregue,
    ModalAtestadoPendente,
    ModalAniversariantes,
    ModalPautaJulgamento,
    ModalViewPdf,
    ModalJogadorDetalhes,
    ModalJogosAdiados,
    ModalJogosWo,
    ModalNoticia
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CompeticaoService,
    Connectivity,
    ImgcacheService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Network,
    SingletonService,
    NativeStorage,
    OneSignal,
    FileTransfer,
    FileTransferObject,
  ]
})
export class AppModule {}
