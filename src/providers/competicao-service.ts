import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { Connectivity } from './connectivity';
import { SingletonService } from './singleton';

/*
  Generated class for the CompeticaoService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CompeticaoService {

  public pageElement: any;
  public data: Observable<any>;
  public initilized: boolean=false;
  public loaded: boolean=false;

  constructor(public http: Http, private conn: Connectivity, public singleton: SingletonService) {

  }

  init(pageElement: any): Promise<any>{
    return this.loadedCompeticao();
  }

  loadedCompeticao(): Promise<any> {
    return new Promise((resolve)=>{
      if(this.conn.isOnline){
        this.initApp();
        resolve(true);
      }
    });
  }

  initApp(): Promise<any>{
    this.initilized = true;
    return new Promise((resolve)=>{
      resolve(true);
    });
  }

  findAll(): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/competicao")
    .map(res=>res.json());
  }

  findOne(pk): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/competicao-one/pk/"+pk)
    .map(res=>res.json());
  }

  findBanner(): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/banner_principal")
    .map(res => res.json());
  }

  findCategoria(competicao): Observable<any>{
      return this.http.get(this.singleton.urlBase+"service/list/model/categoria/competicao/"+competicao)
      .map(res => res.json());
  }

  findNoticias(competicao,limit:number = 10,skip:number = 0): Observable<any>{
      return this.http.get(this.singleton.urlBase+"service/list/model/noticias/id/"+competicao+"/limit/"+limit+"/skip/"+skip)
      .map(res => res.json());
  }

  findPartida(competicao): Observable<any>{
      return this.http.get(this.singleton.urlBase+"service/list/model/partida/competicao/"+competicao)
      .map(res => res.json());
  }

  findClassificacao(competicao,categoria): Observable<any>{
      return this.http.get(this.singleton.urlBase+"service/list/model/classificacao/competicao/"+competicao+"/categoria/"+categoria)
      .map(res => res.json());
  }

  findTabelaJogos(competicao,categoria,rodada): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/tblJogos/competicao/"+competicao+"/categoria/"+categoria+"/rodada/"+rodada)
    .map(res=>res.json())
  }

  findJogadoresEquipe(equipe): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/jogadoresEquipe/equipe/"+equipe)
    .map(res=>res.json())
  }

  findJogosEquipe(equipe): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/jogosEquipe/equipe/"+equipe)
    .map(res=>res.json())
  }

  findDetalhesJogo(jogo): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/detalhesJogo/jogo/"+jogo)
    .map(res=>res.json())
  }

  findGoleiro(goleiro): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/goleiro/goleiro/"+goleiro)
    .map(res=>res.json())
  }

  findJogadoresSuspensos(id): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/jogadoressuspensos/id/"+id)
    .map(res=>res.json())
  }
  findJogadoresPendurados(id): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/jogadorespendurados/id/"+id)
    .map(res=>res.json())
  }
  findBalancouRede(id): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/balancourede/id/"+id)
    .map(res=>res.json())
  }
  findAtestadoPendente(id): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/atestadopendente/id/"+id)
    .map(res=>res.json())
  }
  findAtestadoEntregue(id): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/atestadoentregue/id/"+id)
    .map(res=>res.json())
  }
  findMembro1(id): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/membro1/id/"+id)
    .map(res=>res.json())
  }
  findMembro2(id): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/membro2/id/"+id)
    .map(res=>res.json())
  }
  findPautaJulgamento(id): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/pautajulgamento/id/"+id)
    .map(res=>res.json())
  }
  findAniversariantes(id): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/aniversariantes/id/"+id)
    .map(res=>res.json())
  }
  findJogadorDetalhes(nome,categoria,id): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/jogadordetalhes/nome/"+nome+"/categoria/"+categoria+"/id/"+id)
    .map(res=>res.json())
  }
  findJogosAdiados(id): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/jogosAdiados/id/"+id)
    .map(res=>res.json())
  }
  findJogosWo(id): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/jogosWo/id/"+id)
    .map(res=>res.json())
  }
  findEquipe(id): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/equipe/id/"+id)
    .map(res=>res.json())
  }

  findNew(): Observable<any>{
    return this.http.get(this.singleton.urlBase+"service/list/model/competicao-new/")
    .map(res=>res.json())
  }
}
