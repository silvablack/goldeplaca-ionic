import { Injectable } from '@angular/core';

@Injectable()
export class SingletonService{
  public urlBase:any = 'http://www.goldeplacama.com.br/';
  public urlService: any = 'http://www.goldeplacama.com.br/service/';
  public urlEquipeImagem: any = 'http://www.goldeplacama.com.br/images/logo/';
  public urlCompeticaoImagem: any = 'http://www.goldeplacama.com.br/';
  public urlJogadorImagem: any = 'http://www.goldeplacama.com.br/images/jogadores/perfil/';
  public urlLocalhost:any  = 'http://localhost/';
}
