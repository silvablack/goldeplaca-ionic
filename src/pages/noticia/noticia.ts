import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { CompeticaoService } from '../../providers/competicao-service';
import 'rxjs/add/operator/finally';
import { ModalNoticia } from '../../components/modals/simplepage/ModalNoticia';

@Component({
    templateUrl: 'noticia.html',
    providers:[CompeticaoService]
})

export class Noticia {
    posts: Array<any> = [];
    idCompeticao: number;
    limit: number = 10;
    skip: number = 0;
    loading;
    constructor(public competicaoService: CompeticaoService, public nav:NavParams, public modal: ModalController, public load: LoadingController){
      this.idCompeticao = nav.get('data');

      this.loadPosts();
    }


    loadPosts(){
      this.competicaoService.findNoticias(this.idCompeticao,this.limit,this.skip)
      .subscribe((then)=>{
        console.log(then);
        this.posts = this.posts.concat(then);
        console.table({concat: this.posts});
      },(error)=>console.log(error));
    }

    openNoticia(post){
      let modal = this.modal.create(ModalNoticia,{noticia:post});
      modal.present();
    }

    doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    this.skip += 5;
    let loading = this.load.create({content:'Carregando...'});
    loading.present();
      setTimeout(() => {
        this.loadPosts();
        console.table({setTimeout: this.posts});
        console.log('Async operation has ended');
        infiniteScroll.complete();
        loading.dismiss();
      }, 200);

  }
}
