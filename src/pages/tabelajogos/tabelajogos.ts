import { Component } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/finally';
import { NavController, NavParams, ModalController, LoadingController, Events} from 'ionic-angular';
import { CompeticaoService } from '../../providers/competicao-service';
import { ModalDetalhesJogo } from '../../components/modals/jogos/ModalDetalhesJogo';
import { Home } from '../home/home';
/**
 * Generated class for the TabelajogosPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-tabelajogos',
  templateUrl: 'tabelajogos.html',
})
export class TabelaJogos {
  public jogos: Array<any>;
  public rodadas: Array<any>;
  public rodadaview: string;
  public categoria: string;
  public competicao: any;
  public load;
  constructor(public events: Events, public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams, public competicaoService: CompeticaoService,public modalCtrl: ModalController) {

    console.log('constructor tabela jogos');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabelajogosPage');
    this.categoria = this.navParams.get('categoria');
    this.competicao = this.navParams.get('competicao');
    this.filterRodada(null);

  }

  filterRodada(rodada = null){
    let load =  this.loadingCtrl.create({content: 'Carregando...'});
    load.present();
    //Se variavel rodada = null entao a busca sera de todas as rodadas
    this.competicaoService.findTabelaJogos(this.competicao,this.categoria,rodada)
    .finally(()=>{
      load.dismiss();
    })
    .subscribe((data)=>{
      this.rodadaview = data[0].rodada_view.toString().replace(/&ORDF;/gi,'ª');
      this.jogos = data[0].jogos;
      this.rodadas = data[0].list_rodadas;
    });
    /*console.log(rodada);
    let jogos: Array<any>;
    Observable.from(this.jogos)
    .filter((x:any)=> x.rodada = rodada)
    .subscribe(data => {
      jogos.concat(data);
    })
    console.log(jogos);*/
  }

  //RESOLVENDO BUG QUANDO EXISTE APENAS UMA CATEGORIA E NAO DA PRA USAR A ACAO ionChange
  ionViewWillLeave(){
    this.competicaoService.findOne(this.competicao)
    .subscribe((competicao)=>{
      this.events.publish('competition:selected', competicao[0].id,competicao[0].nome_competicao);
        this.navCtrl.setRoot(Home, {
          item: competicao[0].id
        });
    });

  }

  detalhes(id: number){
    this.events.publish('detalhesjogos:selected',id);
    /*let loader = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    loader.present();

    this.competicaoService.findDetalhesJogo(id).finally(()=>{
      loader.dismiss();
    }).subscribe((data)=>{
      console.table({'detalheJogo':data});
      this.modalCtrl.create(ModalDetalhesJogo,{jogo:data}).present();
    }, (err) =>{
      console.log(err);
    })*/
  }

}
