import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { CompeticaoService } from '../../providers/competicao-service';
import { Competicao } from '../competicao/competicao';
import { Home } from '../home/home';
import { NativeStorage } from '@ionic-native/native-storage';
import 'rxjs/add/operator/finally';
/**
 * Generated class for the ConfigPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-config',
  templateUrl: 'config.html',
})
export class ConfigPage {
  currentCompetitions: any = [];
  allCompetitions: any = [];
  newCompetitions: any = [];
  idsCompetitions = [];
  IDs: any = [];
  constructor(public loadingCtrl:LoadingController,public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public nativeStorage: NativeStorage, public compService: CompeticaoService) {

  }

  ionViewDidLoad() {
    let load = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    load.present();

    this.nativeStorage.getItem('current_competitions')
    .then((competicoes)=>{
      for(let competicao of competicoes){
        this.compService.findOne(competicao.id)
        .subscribe((current)=>{
          this.currentCompetitions.push(current[0]);
          this.IDs.push({id: current[0].id, name:current[0].nome_competicao});
          //console.log(this.currentCompetitions);
        },(error)=>{
          console.log(error);
        });
      }
      this.compService.findAll().finally(()=>{
        load.dismiss();
      }).subscribe((competicoes)=>{
        this.allCompetitions = competicoes;
      },(error)=>{console.log(error)});
    },(error)=>{
      console.log(error);
    });
  }

  //BUSCAR ID E RETORNAR TRUE - CHECKED CHECKBOX
  searchArray(array_id): boolean{
    for(let i of this.IDs){
      return i.id === array_id;
    }
    return false;
  }

  pushCompeticao(id,name,$event){
    //console.table({'id':id, 'event':$event});
    console.log($event.checked);
    this.newCompetitions.push({id: id, name: name});
    //console.log(this.competitionsSelected);
  }

  storageCompetitions(){
    if(this.newCompetitions){
      this.nativeStorage.clear().then((value) => {
        this.nativeStorage.setItem('current_competitions',this.newCompetitions);
        let alert = this.alertCtrl.create({
          title: 'Atenção',
          message: 'Caso queira realizar qualquer alteração na visualização de competições, vá em Menu > Alterar Competições',
          buttons: ['Ok']
        });
        alert.present();
        this.navCtrl.setRoot(Competicao);
      });
    }else{
        this.alertCtrl.create({
          title: 'Atenção',
          message: 'Por favor, escolha uma alternativa',
          buttons: ['Ok']
        }).present();
    }
  }

}
