import { Component } from '@angular/core';
import { LoadingController, AlertController, NavController, NavParams } from 'ionic-angular';
import { CompeticaoService } from '../../providers/competicao-service';
import { Competicao } from '../competicao/competicao';
import { NativeStorage } from '@ionic-native/native-storage';
import 'rxjs/add/operator/finally';

/**
 * Generated class for the PreHomePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-pre-home',
  templateUrl: 'pre-home.html',
})
export class PreHome {
  competicoes: Array<any>;
  competitionsSelected = [];
  load: any;
  constructor(public alertCtrl: AlertController, public storage: NativeStorage, public loading: LoadingController, public competicaoService: CompeticaoService, public navCtrl: NavController, public navParams: NavParams) {
    this.load = loading.create({
      content: "Carregando..."
    });
    this.load.present();
  }

  ionViewDidLoad() {
    this.competicaoService.findAll().finally(()=>{
      this.load.dismiss();
    }).subscribe((competicoes)=>{
      this.competicoes = competicoes;
      for(let x of competicoes){
        this.pushCompeticao(x.attr.id,x.attr.nome_competicao,null);
      }
      this.storageCompetitions();
    },(error)=>{console.log(error)});

    console.log('ionViewDidLoad PreHomePage');
  }

  pushCompeticao(id,name,$event){
    //console.table({'id':id, 'event':$event});
    this.competitionsSelected.push({id: id, name: name});
    //console.log(this.competitionsSelected);
  }

  storageCompetitions(){
    if(this.competitionsSelected){
      let _ids = this.competitionsSelected;
      this.storage.setItem('current_competitions',_ids);
      this.navCtrl.setRoot(Competicao);
    }else{
        this.alertCtrl.create({
          title: 'Atenção',
          message: 'Por favor, escolha uma alternativa',
          buttons: ['Ok']
        }).present();
    }
  }

}
